#!/usr/bin/env python
import datetime
from argparse import ArgumentParser
from pathlib import Path
from typing import List, Dict

from GammaHeatingControl.protocol import parse_frame, parse_frame_data, FrameType


def print_diff_in_frames(frames_to_diff: List[Dict]) -> None:
    """
    A function to abstract the diff printing of a specific sequence of frames.
    """
    this_frame = frames_to_diff[0]
    try:
        parsed_frame_data = parse_frame_data(this_frame)
    except ValueError:
        parsed_frame_data = None

    print("ts: {0} hex: {1} parsed: {2}".format(
        str(this_frame.get("timestamp")).ljust(30),
        this_frame.get("data").hex().upper(),
        parsed_frame_data))
    old_raw = frames_to_diff[0].get("data")
    for this_frame in frames_to_diff[1:]:
        raw = this_frame.get("data")
        if raw != old_raw:
            if len(raw) == len(old_raw):  # problem with different types right now
                diff = "".join(("{0:02X}".format(x) if (old_raw[idx] != x) else "  " for idx, x in enumerate(raw)))
                diff_idx = [idx for idx, x in enumerate(raw) if (old_raw[idx] != x)]
                try:
                    parsed_frame_data = parse_frame_data(this_frame)
                except ValueError:
                    parsed_frame_data = None
                print("ts: {0} hex: {1} indices: {2} changed, parsed: {3}".format(
                    str(this_frame.get("timestamp")).ljust(30),
                    diff,
                    diff_idx,
                    parsed_frame_data))
                old_raw = raw


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("log_file", type=Path)
    parser.add_argument("-s", dest="sender", type=lambda x: int(x, 16), default=None)
    parser.add_argument("-r", dest="receiver", type=lambda x: int(x, 16), default=None)
    parser.add_argument("-d", dest="show_diff", action="store_true")
    parser.add_argument("-t", dest="frame_type", type=int, default=None)
    args = parser.parse_args()

    frames = list()
    with args.log_file.open() as fp:
        for line in fp.readlines():
            # print(line)
            date_, time_, file_, level_, raw_frame = line.split()
            timestamp = " ".join((date_, time_))
            frame = parse_frame(bytes.fromhex(raw_frame))
            frame.update({"timestamp": datetime.datetime.fromisoformat(timestamp)})
            frames.append(frame)

    senders_receiver_pairs = set(
        (frame.get("sender"), frame.get("receiver")) for frame in frames)

    if args.sender is not None:
        senders_receiver_pairs = set(
            (sender, receiver) for sender, receiver in senders_receiver_pairs if sender == args.sender)
    if args.receiver is not None:
        senders_receiver_pairs = set(
            (sender, receiver) for sender, receiver in senders_receiver_pairs if receiver == args.receiver)

    for sender, receiver in senders_receiver_pairs:
        frames_from_sender_to_receiver = [frame for frame in frames if
                                          frame.get("receiver") == receiver and frame.get("sender") == sender]

        if args.frame_type is not None:
            frame_types_from_sender_to_receiver = {FrameType(args.frame_type)}
        else:
            frame_types_from_sender_to_receiver = set([frame.get("type") for frame in frames_from_sender_to_receiver])
        # if args.exclude_frame is not None:
        #     frame_types_from_sender_to_receiver = set(
        #         [frame_type for frame_type in frame_types_from_sender_to_receiver if
        #          frame_type not in args.exclude_frame])

        for frame_type in frame_types_from_sender_to_receiver:
            frames_of_a_specific_type = [frame for frame in frames_from_sender_to_receiver if
                                         frame.get("type") == frame_type]
            length_variants_of_a_specific_frame_type = set(
                [len(frame.get("data")) for frame in frames_of_a_specific_type])
            for frame_length_variant in length_variants_of_a_specific_frame_type:
                frames_of_a_specific_type_and_a_specific_length = [frame for frame in frames_of_a_specific_type if
                                                                   len(frame.get("data")) == frame_length_variant]
                print("{0.name} -> {1.name} : {2.name} : length {3}".format(sender, receiver, frame_type,
                                                                            frame_length_variant))
                if args.show_diff is True:
                    print_diff_in_frames(frames_of_a_specific_type_and_a_specific_length)
